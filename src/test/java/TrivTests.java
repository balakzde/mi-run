import org.junit.Test;

public class TrivTests {

    private void runTest(String file) throws Exception {
        String[] args1 = new String[2];
        args1[0] = file;
        args1[1] = "-Xmx6m";
        Interpreter.main(args1);
    }

    @Test
    public void testIncrement() throws Exception {
        runTest("src/test/resources/Increment.java");
    }

    @Test
    public void testDecrement() throws Exception {
        runTest("src/test/resources/Decrement.java");
    }

    @Test
    public void testWhile() throws Exception {
        runTest("src/test/resources/While.java");
    }

    @Test
    public void testFori() throws Exception {
        runTest("src/test/resources/Fori.java");
    }

    @Test
    public void testBreak() throws Exception {
        runTest("src/test/resources/Break.java");
    }
    @Test
    public void testContinue() throws Exception {
        runTest("src/test/resources/Continue.java");
    }
}
