import org.junit.Test;

public class ExampleTests {

    private void runTest(String file) throws Exception {
        String[] args1 = new String[2];
        args1[0] = file;
        args1[1] = "-Xmx6m";
        Interpreter.main(args1);
    }

    @Test
    public void testTribonacci() throws Exception {
        runTest("src/test/resources/Tribonacci.java");
    }

    @Test
    public void recursiveFibonacci() throws Exception {
        runTest("src/test/resources/RecursiveFibonacci.java");
    }
    @Test
    public void gcd() throws Exception {
        runTest("src/test/resources/GCD.java");
    }
}
