
public class GCD {

    public static void main(String[] args) {
        println("gcd of 10 and 4: " + gcd(10, 4));
        println("gcd of 7 and 11: " + gcd(7, 11));
        println("gcd of 50 and 20: " + gcd(50, 20));
        println("gcd of 186 and 156: " + gcd(186, 156));
        println("gcd of 23 and 4: " + gcd(23, 4));
    }

    public static int gcd(int a, int b) {
        if (b == 0) {
            return a;
        }
        return gcd(b, a % b);
    }
}
