public class Continue {

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            if(i%3==0){
                println("CONTINUING");
                continue;
            }
            println("Printing i in fori, should continue when i%3==0: " + i);
        }
    }
}
