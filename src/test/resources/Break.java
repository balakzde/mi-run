public class Break {

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            println("Printing i in fori, should break when i==5: " + i);
            if(i==5){
                println("BREAKING");
                break;
            }
        }
    }
}
