public class While {

    public static void main(String[] args) {
        int i = 0;
        while (i < 10) {
            println("Printing i in while, should be 10 iterations: " + i);
            i++;
        }
    }
}
