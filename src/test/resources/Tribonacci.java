public class Tribonacci {

	public static void main(String[] args) {
		Integer a = 0;
		Integer b = 0;
		Integer c = 1;

		int i = 1;
		while (c >= 0) {
			print(i + ": ");
			println(c);
			int d = c;
			c = a + b + c;
			a = b;
			b = d;
			i++;
		}
	}
}
