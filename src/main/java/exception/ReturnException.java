package exception;

import memory.StackEntry;

public class ReturnException extends RuntimeException {
    private StackEntry returnValue;

    public ReturnException() {
    }

    public ReturnException(StackEntry value){
        super();
        returnValue = value;
    }

    public StackEntry getReturnValue() {
        return returnValue;
    }
}
