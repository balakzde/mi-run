package util;

import memory.Heap;
import memory.ActivationEntry;
import memory.StackEntry;

public class IntegerOperations {
    public static int createInteger(int value, ActivationEntry activationEntry) {
        Heap heap = Heap.getInstance();
        int reference = heap.allocate(activationEntry);
        heap.setValue(reference, value);
        return reference;
    }

    public static void updateValue(StackEntry stackEntry, int value, ActivationEntry activationEntry) {
        stackEntry.setValue(0);
        stackEntry.setValue(createInteger(value, activationEntry));
    }
}
