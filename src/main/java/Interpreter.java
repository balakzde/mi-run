import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import memory.Heap;
import memory.MethodIndex;
import memory.ActivationEntry;
import visitor.EvaluationVisitor;
import visitor.IndexingVisitor;

import java.io.FileInputStream;
import java.io.InputStream;
import java.text.NumberFormat;

public class Interpreter {

    public static void interpret(InputStream in, int heapSize) {
        CompilationUnit cu = JavaParser.parse(in);
        cu.accept(new IndexingVisitor(), null);
        Heap.getInstance().init(heapSize);
        MethodIndex.getInstance().getMainNode().getBody().get().accept(new EvaluationVisitor(), new ActivationEntry());
    }

    final static String heapSizeOption = "-Xmx";

    public static void main(String[] args) throws Exception {
        InputStream in = null;
        long heapSize = 1024;
        for (int i = 0; i < args.length; ++i) {
            if (args[i].matches(heapSizeOption + ".*")){
                String size = args[i].substring(heapSizeOption.length());
                heapSize = NumberFormat.getInstance().parse(size).longValue();
                switch(size.charAt(size.length()-1)) {
                    case 'g' :
                        heapSize *= 1024;
                    case 'm' :
                        heapSize *= 1024;
                    case 'k' :
                        heapSize *= 1024;
                }
            } else {
                in = new FileInputStream(args[i]);
            }
        }
        heapSize /= 4;
        if (heapSize > Integer.MAX_VALUE) {
            throw new IllegalArgumentException("heap size too big");
        }
        if (in == null) {
            in = System.in;
        }
        interpret(in, (int) heapSize); //todo memory size
    }
}