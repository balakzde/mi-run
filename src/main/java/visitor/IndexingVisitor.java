package visitor;


import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.Parameter;
import memory.MethodIndex;
import visitor.dummy.IndexingDummyVisitor;

import java.util.ArrayList;
import java.util.List;

public class IndexingVisitor extends IndexingDummyVisitor {

    private static DataTypeVisitor dataTypeVisitor = new DataTypeVisitor();

    @Override
    public void visit(CompilationUnit n, Void arg) {
        for (Node node : n.getChildNodes()) {
            node.accept(this, null);
        }
    }

    @Override
    public void visit(ClassOrInterfaceDeclaration n, Void arg) {
        for (Node node : n.getChildNodes()) {
            node.accept(this, null);
        }
    }

    @Override
    public void visit(MethodDeclaration n, Void arg) {
        if (!n.getModifiers().contains(Modifier.STATIC)) {
            throw new RuntimeException("nonstatic methods not supported");
        }
        if (n.getNameAsString().equals("main")) {
            MethodIndex.getInstance().setMainNode(n);
        } else {
            List<String> parameterTypes = new ArrayList<>();
            for (Parameter parameter : n.getParameters()) {
                String dataType = parameter.getType().accept(dataTypeVisitor, null);
                parameterTypes.add(dataType);
            }
            MethodIndex.getInstance().addMethod(parameterTypes, n);
        }
    }
}
