package visitor;

import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.ast.type.PrimitiveType;
import visitor.dummy.DataTypeDummyVisitor;

public class DataTypeVisitor extends DataTypeDummyVisitor {
    @Override
    public String visit(ClassOrInterfaceType n, Void arg) {
        return n.getNameAsString();
    }

    @Override
    public String visit(PrimitiveType n, Void arg) {
        return n.getType().asString();
    }
}
