package visitor.dummy;

import com.github.javaparser.ast.*;
import com.github.javaparser.ast.body.*;
import com.github.javaparser.ast.comments.BlockComment;
import com.github.javaparser.ast.comments.JavadocComment;
import com.github.javaparser.ast.comments.LineComment;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.stmt.*;
import com.github.javaparser.ast.type.*;
import com.github.javaparser.ast.visitor.VoidVisitor;

public abstract class IndexingDummyVisitor implements VoidVisitor<Void> {
    @Override
    public void visit(PackageDeclaration n, Void arg) {

    }

    @Override
    public void visit(TypeParameter n, Void arg) {

    }

    @Override
    public void visit(LineComment n, Void arg) {

    }

    @Override
    public void visit(BlockComment n, Void arg) {

    }

    @Override
    public void visit(EnumDeclaration n, Void arg) {

    }

    @Override
    public void visit(EnumConstantDeclaration n, Void arg) {

    }

    @Override
    public void visit(AnnotationDeclaration n, Void arg) {

    }

    @Override
    public void visit(AnnotationMemberDeclaration n, Void arg) {

    }

    @Override
    public void visit(FieldDeclaration n, Void arg) {

    }

    @Override
    public void visit(VariableDeclarator n, Void arg) {

    }

    @Override
    public void visit(ConstructorDeclaration n, Void arg) {

    }

    @Override
    public void visit(Parameter n, Void arg) {

    }

    @Override
    public void visit(EmptyMemberDeclaration n, Void arg) {

    }

    @Override
    public void visit(InitializerDeclaration n, Void arg) {

    }

    @Override
    public void visit(JavadocComment n, Void arg) {

    }

    @Override
    public void visit(ClassOrInterfaceType n, Void arg) {

    }

    @Override
    public void visit(PrimitiveType n, Void arg) {

    }

    @Override
    public void visit(ArrayType n, Void arg) {

    }

    @Override
    public void visit(ArrayCreationLevel n, Void arg) {

    }

    @Override
    public void visit(IntersectionType n, Void arg) {

    }

    @Override
    public void visit(UnionType n, Void arg) {

    }

    @Override
    public void visit(VoidType n, Void arg) {

    }

    @Override
    public void visit(WildcardType n, Void arg) {

    }

    @Override
    public void visit(UnknownType n, Void arg) {

    }

    @Override
    public void visit(ArrayAccessExpr n, Void arg) {

    }

    @Override
    public void visit(ArrayCreationExpr n, Void arg) {

    }

    @Override
    public void visit(ArrayInitializerExpr n, Void arg) {

    }

    @Override
    public void visit(AssignExpr n, Void arg) {

    }

    @Override
    public void visit(BinaryExpr n, Void arg) {

    }

    @Override
    public void visit(CastExpr n, Void arg) {

    }

    @Override
    public void visit(ClassExpr n, Void arg) {

    }

    @Override
    public void visit(ConditionalExpr n, Void arg) {

    }

    @Override
    public void visit(EnclosedExpr n, Void arg) {

    }

    @Override
    public void visit(FieldAccessExpr n, Void arg) {

    }

    @Override
    public void visit(InstanceOfExpr n, Void arg) {

    }

    @Override
    public void visit(StringLiteralExpr n, Void arg) {

    }

    @Override
    public void visit(IntegerLiteralExpr n, Void arg) {

    }

    @Override
    public void visit(LongLiteralExpr n, Void arg) {

    }

    @Override
    public void visit(CharLiteralExpr n, Void arg) {

    }

    @Override
    public void visit(DoubleLiteralExpr n, Void arg) {

    }

    @Override
    public void visit(BooleanLiteralExpr n, Void arg) {

    }

    @Override
    public void visit(NullLiteralExpr n, Void arg) {

    }

    @Override
    public void visit(MethodCallExpr n, Void arg) {

    }

    @Override
    public void visit(NameExpr n, Void arg) {

    }

    @Override
    public void visit(ObjectCreationExpr n, Void arg) {

    }

    @Override
    public void visit(ThisExpr n, Void arg) {

    }

    @Override
    public void visit(SuperExpr n, Void arg) {

    }

    @Override
    public void visit(UnaryExpr n, Void arg) {

    }

    @Override
    public void visit(VariableDeclarationExpr n, Void arg) {

    }

    @Override
    public void visit(MarkerAnnotationExpr n, Void arg) {

    }

    @Override
    public void visit(SingleMemberAnnotationExpr n, Void arg) {

    }

    @Override
    public void visit(NormalAnnotationExpr n, Void arg) {

    }

    @Override
    public void visit(MemberValuePair n, Void arg) {

    }

    @Override
    public void visit(ExplicitConstructorInvocationStmt n, Void arg) {

    }

    @Override
    public void visit(LocalClassDeclarationStmt n, Void arg) {

    }

    @Override
    public void visit(AssertStmt n, Void arg) {

    }

    @Override
    public void visit(BlockStmt n, Void arg) {

    }

    @Override
    public void visit(LabeledStmt n, Void arg) {

    }

    @Override
    public void visit(EmptyStmt n, Void arg) {

    }

    @Override
    public void visit(ExpressionStmt n, Void arg) {

    }

    @Override
    public void visit(SwitchStmt n, Void arg) {

    }

    @Override
    public void visit(SwitchEntryStmt n, Void arg) {

    }

    @Override
    public void visit(BreakStmt n, Void arg) {

    }

    @Override
    public void visit(ReturnStmt n, Void arg) {

    }

    @Override
    public void visit(IfStmt n, Void arg) {

    }

    @Override
    public void visit(WhileStmt n, Void arg) {

    }

    @Override
    public void visit(ContinueStmt n, Void arg) {

    }

    @Override
    public void visit(DoStmt n, Void arg) {

    }

    @Override
    public void visit(ForeachStmt n, Void arg) {

    }

    @Override
    public void visit(ForStmt n, Void arg) {

    }

    @Override
    public void visit(ThrowStmt n, Void arg) {

    }

    @Override
    public void visit(SynchronizedStmt n, Void arg) {

    }

    @Override
    public void visit(TryStmt n, Void arg) {

    }

    @Override
    public void visit(CatchClause n, Void arg) {

    }

    @Override
    public void visit(LambdaExpr n, Void arg) {

    }

    @Override
    public void visit(MethodReferenceExpr n, Void arg) {

    }

    @Override
    public void visit(TypeExpr n, Void arg) {

    }

    @Override
    public void visit(NodeList n, Void arg) {

    }

    @Override
    public void visit(ImportDeclaration n, Void arg) {

    }

    @Override
    public void visit(Name n, Void arg) {

    }

    @Override
    public void visit(SimpleName n, Void arg) {

    }
}
