package visitor.dummy;

import com.github.javaparser.ast.*;
import com.github.javaparser.ast.body.*;
import com.github.javaparser.ast.comments.BlockComment;
import com.github.javaparser.ast.comments.JavadocComment;
import com.github.javaparser.ast.comments.LineComment;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.stmt.*;
import com.github.javaparser.ast.type.*;
import com.github.javaparser.ast.visitor.GenericVisitor;

public abstract class DataTypeDummyVisitor implements GenericVisitor<String, Void> {
    @Override
    public String visit(CompilationUnit n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(PackageDeclaration n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(TypeParameter n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(LineComment n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(BlockComment n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ClassOrInterfaceDeclaration n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(EnumDeclaration n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(EnumConstantDeclaration n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(AnnotationDeclaration n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(AnnotationMemberDeclaration n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(FieldDeclaration n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(VariableDeclarator n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ConstructorDeclaration n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(MethodDeclaration n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(Parameter n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(EmptyMemberDeclaration n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(InitializerDeclaration n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(JavadocComment n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ArrayType n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ArrayCreationLevel n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(IntersectionType n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(UnionType n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(VoidType n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(WildcardType n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(UnknownType n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ArrayAccessExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ArrayCreationExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ArrayInitializerExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(AssignExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(BinaryExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(CastExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ClassExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ConditionalExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(EnclosedExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(FieldAccessExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(InstanceOfExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(StringLiteralExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(IntegerLiteralExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(LongLiteralExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(CharLiteralExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(DoubleLiteralExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(BooleanLiteralExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(NullLiteralExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(MethodCallExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(NameExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ObjectCreationExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ThisExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(SuperExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(UnaryExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(VariableDeclarationExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(MarkerAnnotationExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(SingleMemberAnnotationExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(NormalAnnotationExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(MemberValuePair n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ExplicitConstructorInvocationStmt n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(LocalClassDeclarationStmt n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(AssertStmt n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(BlockStmt n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(LabeledStmt n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(EmptyStmt n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ExpressionStmt n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(SwitchStmt n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(SwitchEntryStmt n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(BreakStmt n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ReturnStmt n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(IfStmt n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(WhileStmt n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ContinueStmt n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(DoStmt n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ForeachStmt n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ForStmt n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ThrowStmt n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(SynchronizedStmt n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(TryStmt n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(CatchClause n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(LambdaExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(MethodReferenceExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(TypeExpr n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(NodeList n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(Name n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(SimpleName n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ImportDeclaration n, Void arg) {
        throw new RuntimeException("Unsupported operation");
    }
}
