package visitor.dummy;

import com.github.javaparser.ast.*;
import com.github.javaparser.ast.body.*;
import com.github.javaparser.ast.comments.BlockComment;
import com.github.javaparser.ast.comments.JavadocComment;
import com.github.javaparser.ast.comments.LineComment;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.stmt.*;
import com.github.javaparser.ast.type.*;
import com.github.javaparser.ast.visitor.GenericVisitor;
import memory.ActivationEntry;


public abstract class StringExpressionDummyVisitor implements GenericVisitor<String, ActivationEntry> {
    @Override
    public String visit(CompilationUnit n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(PackageDeclaration n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported Operation");
    }

    @Override
    public String visit(TypeParameter n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported Operation");
    }

    @Override
    public String visit(LineComment n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(BlockComment n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ClassOrInterfaceDeclaration n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(EnumDeclaration n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(EnumConstantDeclaration n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(AnnotationDeclaration n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(AnnotationMemberDeclaration n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(FieldDeclaration n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(VariableDeclarator n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ConstructorDeclaration n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(MethodDeclaration n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(Parameter n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(EmptyMemberDeclaration n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(InitializerDeclaration n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(JavadocComment n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ClassOrInterfaceType n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(PrimitiveType n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ArrayType n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ArrayCreationLevel n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(IntersectionType n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(UnionType n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(VoidType n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(WildcardType n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(UnknownType n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ArrayAccessExpr n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ArrayCreationExpr n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ArrayInitializerExpr n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(AssignExpr n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(CastExpr n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ClassExpr n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ConditionalExpr n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(FieldAccessExpr n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(InstanceOfExpr n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(LongLiteralExpr n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(CharLiteralExpr n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(DoubleLiteralExpr n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ObjectCreationExpr n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ThisExpr n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(SuperExpr n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(VariableDeclarationExpr n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(MarkerAnnotationExpr n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(SingleMemberAnnotationExpr n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(NormalAnnotationExpr n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(MemberValuePair n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ExplicitConstructorInvocationStmt n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(LocalClassDeclarationStmt n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(AssertStmt n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(BlockStmt n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(LabeledStmt n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(EmptyStmt n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ExpressionStmt n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(SwitchStmt n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(SwitchEntryStmt n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(BreakStmt n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ReturnStmt n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(IfStmt n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(WhileStmt n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ContinueStmt n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(DoStmt n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ForeachStmt n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ForStmt n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ThrowStmt n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(SynchronizedStmt n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(TryStmt n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(CatchClause n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(LambdaExpr n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(MethodReferenceExpr n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(TypeExpr n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(NodeList n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(Name n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(SimpleName n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public String visit(ImportDeclaration n, ActivationEntry arg) {
        throw new RuntimeException("Unsupported operation");
    }
}
