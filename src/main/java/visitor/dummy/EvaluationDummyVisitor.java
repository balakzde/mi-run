package visitor.dummy;

import com.github.javaparser.ast.*;
import com.github.javaparser.ast.body.*;
import com.github.javaparser.ast.comments.BlockComment;
import com.github.javaparser.ast.comments.JavadocComment;
import com.github.javaparser.ast.comments.LineComment;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.stmt.*;
import com.github.javaparser.ast.type.*;
import com.github.javaparser.ast.visitor.GenericVisitor;
import memory.ActivationEntry;
import memory.StackEntry;


public abstract class EvaluationDummyVisitor implements GenericVisitor<StackEntry, ActivationEntry> {
    
    @Override
    public StackEntry visit(CompilationUnit compilationUnit, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(PackageDeclaration packageDeclaration, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(TypeParameter typeParameter, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(LineComment lineComment, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(BlockComment blockComment, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(ClassOrInterfaceDeclaration classOrInterfaceDeclaration, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(EnumDeclaration enumDeclaration, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(EnumConstantDeclaration enumConstantDeclaration, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(AnnotationDeclaration annotationDeclaration, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(AnnotationMemberDeclaration annotationMemberDeclaration, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(FieldDeclaration fieldDeclaration, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(VariableDeclarator variableDeclarator, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(ConstructorDeclaration constructorDeclaration, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(MethodDeclaration methodDeclaration, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(Parameter parameter, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(EmptyMemberDeclaration emptyMemberDeclaration, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(InitializerDeclaration initializerDeclaration, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(JavadocComment javadocComment, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(ClassOrInterfaceType classOrInterfaceType, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(PrimitiveType primitiveType, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(ArrayType arrayType, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(ArrayCreationLevel arrayCreationLevel, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(IntersectionType intersectionType, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(UnionType unionType, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(VoidType voidType, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(WildcardType wildcardType, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(UnknownType unknownType, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(ArrayAccessExpr arrayAccessExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(ArrayCreationExpr arrayCreationExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(ArrayInitializerExpr arrayInitializerExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(AssignExpr assignExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(BinaryExpr binaryExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(CastExpr castExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(ClassExpr classExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(ConditionalExpr conditionalExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(EnclosedExpr enclosedExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(FieldAccessExpr fieldAccessExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(InstanceOfExpr instanceOfExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(StringLiteralExpr stringLiteralExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(IntegerLiteralExpr integerLiteralExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(LongLiteralExpr longLiteralExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(CharLiteralExpr charLiteralExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(DoubleLiteralExpr doubleLiteralExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(BooleanLiteralExpr booleanLiteralExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(NullLiteralExpr nullLiteralExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(MethodCallExpr methodCallExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(NameExpr nameExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(ObjectCreationExpr objectCreationExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(ThisExpr thisExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(SuperExpr superExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(UnaryExpr unaryExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(VariableDeclarationExpr variableDeclarationExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(MarkerAnnotationExpr markerAnnotationExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(SingleMemberAnnotationExpr singleMemberAnnotationExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(NormalAnnotationExpr normalAnnotationExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(MemberValuePair memberValuePair, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(ExplicitConstructorInvocationStmt explicitConstructorInvocationStmt, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(LocalClassDeclarationStmt localClassDeclarationStmt, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(AssertStmt assertStmt, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(BlockStmt blockStmt, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(LabeledStmt labeledStmt, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(EmptyStmt emptyStmt, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(ExpressionStmt expressionStmt, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(SwitchStmt switchStmt, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(SwitchEntryStmt switchEntryStmt, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(BreakStmt breakStmt, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(ReturnStmt returnStmt, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(IfStmt ifStmt, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(WhileStmt whileStmt, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(ContinueStmt continueStmt, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(DoStmt doStmt, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(ForeachStmt foreachStmt, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(ForStmt forStmt, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(ThrowStmt throwStmt, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(SynchronizedStmt synchronizedStmt, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(TryStmt tryStmt, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(CatchClause catchClause, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(LambdaExpr lambdaExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(MethodReferenceExpr methodReferenceExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(TypeExpr typeExpr, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(NodeList nodeList, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(Name name, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(SimpleName simpleName, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }

    @Override
    public StackEntry visit(ImportDeclaration importDeclaration, ActivationEntry activationEntry) {
        throw new RuntimeException("Unsupported operation");
    }
}
