package visitor;

import com.github.javaparser.ast.expr.*;
import memory.ActivationEntry;
import memory.StackEntry;
import visitor.dummy.StringExpressionDummyVisitor;

public class StringExpressionVisitor extends StringExpressionDummyVisitor {

    private static EvaluationVisitor expressionVisitor = new EvaluationVisitor();

    @Override
    public String visit(EnclosedExpr n, ActivationEntry arg) {
        return String.valueOf(n.accept(expressionVisitor, arg).getIntValue());
    }

    @Override
    public String visit(StringLiteralExpr n, ActivationEntry arg) {
        return n.getValue();
    }

    @Override
    public String visit(IntegerLiteralExpr n, ActivationEntry arg) {
        return n.getValue();
    }

    @Override
    public String visit(BooleanLiteralExpr n, ActivationEntry arg) {
        return String.valueOf(n.getValue());
    }

    @Override
    public String visit(NullLiteralExpr n, ActivationEntry arg) {
        return "null";
    }

    @Override
    public String visit(NameExpr n, ActivationEntry arg) {
        StackEntry variable = n.accept(expressionVisitor, arg);
        return variable.getDataType().equals("Integer") && variable.getValue() == 0 ? "null" : String.valueOf(variable.getIntValue());
    }

    @Override
    public String visit(UnaryExpr n, ActivationEntry arg) {
        return String.valueOf(n.accept(expressionVisitor, arg).getIntValue());
    }

    @Override
    public String visit(BinaryExpr n, ActivationEntry arg) {
        if (n.getOperator().equals(BinaryExpr.Operator.PLUS)) {
            return n.getLeft().accept(this, arg) + n.getRight().accept(this, arg);
        }
        throw new RuntimeException("IDontKnowWhatJustHappenedException");
    }

    @Override
    public String visit(MethodCallExpr methodCallExpr, ActivationEntry activationEntry) {
        return String.valueOf(methodCallExpr.accept(expressionVisitor, activationEntry).getIntValue());
    }
}
