package visitor;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.comments.BlockComment;
import com.github.javaparser.ast.comments.LineComment;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.stmt.*;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import exception.BreakException;
import exception.ContinueException;
import exception.ReturnException;
import memory.MethodIndex;
import memory.ActivationEntry;
import util.IntegerOperations;
import memory.StackEntry;
import visitor.dummy.EvaluationDummyVisitor;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


@SuppressWarnings("Since15")
public class EvaluationVisitor extends EvaluationDummyVisitor {

    static Scanner s = new Scanner(System.in);
    static DataTypeVisitor dataTypeVisitor = new DataTypeVisitor();
    static StringExpressionVisitor stringExpressionVisitor = new StringExpressionVisitor();

    @Override
    public StackEntry visit(BinaryExpr n, ActivationEntry activationEntry) {
        final StackEntry left = n.getLeft().accept(this, activationEntry);
        final String dataType = left.getDataType();
        int leftValue = left.getIntValue();
        //int rightValue = n.getRight().accept(this, activationEntry);
        switch (n.getOperator()) {
            case OR:
                if (leftValue != 0) {
                    return new StackEntry(1);
                } else {
                    return n.getRight().accept(this, activationEntry);
                }
            case AND:
                if (leftValue == 0) {
                    return new StackEntry(0);
                } else {
                    return n.getRight().accept(this, activationEntry);
                }
        }

        int rightValue = n.getRight().accept(this, activationEntry).getIntValue();
        switch (n.getOperator()) {
            case BINARY_OR:
                return new StackEntry(leftValue | rightValue);
            case BINARY_AND:
                return new StackEntry(leftValue & rightValue);
            case XOR:
                return new StackEntry(leftValue ^ rightValue);
            case EQUALS:
                return new StackEntry(leftValue == rightValue ? 1 : 0);
            case NOT_EQUALS:
                return new StackEntry(leftValue != rightValue ? 1 : 0);
            case LESS:
                return new StackEntry(leftValue < rightValue ? 1 : 0);
            case GREATER:
                return new StackEntry(leftValue > rightValue ? 1 : 0);
            case LESS_EQUALS:
                return new StackEntry(leftValue <= rightValue ? 1 : 0);
            case GREATER_EQUALS:
                return new StackEntry(leftValue >= rightValue ? 1 : 0);
            case LEFT_SHIFT:
                return new StackEntry(leftValue << rightValue);
            case SIGNED_RIGHT_SHIFT:
                return new StackEntry(leftValue >> rightValue);
            case UNSIGNED_RIGHT_SHIFT:
                return new StackEntry(leftValue >>> rightValue);
            case PLUS:
                if (dataType.equals("Integer")) {
                    return new StackEntry(IntegerOperations.createInteger(leftValue + rightValue, activationEntry), "Integer");
                }
                return new StackEntry(leftValue + rightValue, dataType);
            case MINUS:
                if (dataType.equals("Integer")) {
                    return new StackEntry(IntegerOperations.createInteger(leftValue - rightValue, activationEntry), "Integer");
                }
                return new StackEntry(leftValue - rightValue, dataType);
            case MULTIPLY:
                if (dataType.equals("Integer")) {
                    return new StackEntry(IntegerOperations.createInteger(leftValue * rightValue, activationEntry), "Integer");
                }
                return new StackEntry(leftValue * rightValue, dataType);
            case DIVIDE:
                if (dataType.equals("Integer")) {
                    return new StackEntry(IntegerOperations.createInteger(leftValue / rightValue, activationEntry), "Integer");
                }
                return new StackEntry(leftValue / rightValue, dataType);
            case REMAINDER:
                return new StackEntry(leftValue % rightValue);
        }
        throw new RuntimeException("IDontKnowWhatJustHappenedException");
    }

    @Override
    public StackEntry visit(BooleanLiteralExpr n, ActivationEntry activationEntry) {
        return new StackEntry(n.getValue() ? 1 : 0);
    }

    @Override
    public StackEntry visit(IntegerLiteralExpr n, ActivationEntry activationEntry) {
        return new StackEntry(Integer.valueOf(n.getValue()));
    }

    @Override
    public StackEntry visit(NameExpr n, ActivationEntry activationEntry) {
        return activationEntry.findVariable(n.getName().getIdentifier());
    }

    @Override
    public StackEntry visit(UnaryExpr n, ActivationEntry activationEntry) {
        StackEntry stackEntry = n.getExpression().accept(this, activationEntry);
        int value = stackEntry.getIntValue();
        switch (n.getOperator()) {
            case PLUS:
                return stackEntry;
            case MINUS:
                return new StackEntry(-value, stackEntry.getDataType());
            case PREFIX_INCREMENT:
                if (stackEntry.getDataType().equals("Integer")) {
                    IntegerOperations.updateValue(stackEntry, value + 1, activationEntry);
                } else {
                    stackEntry.setValue(value + 1);
                }
                return stackEntry;
            case PREFIX_DECREMENT:
                if (stackEntry.getDataType().equals("Integer")) {
                    IntegerOperations.updateValue(stackEntry, value - 1, activationEntry);
                } else {
                    stackEntry.setValue(value - 1);
                }
                return stackEntry;
            case LOGICAL_COMPLEMENT:
                return new StackEntry(value == 0 ? 1 : 0, stackEntry.getDataType());
            case BITWISE_COMPLEMENT:
                return new StackEntry(~value, stackEntry.getDataType());
            case POSTFIX_INCREMENT:
                StackEntry tmpIncrement = new StackEntry(value);
                if (stackEntry.getDataType().equals("Integer")) {
                    IntegerOperations.updateValue(stackEntry, value + 1, activationEntry);
                } else {
                    stackEntry.setValue(value + 1);
                }
                return tmpIncrement;
            case POSTFIX_DECREMENT:
                StackEntry tmpDecrement = new StackEntry(value);
                if (stackEntry.getDataType().equals("Integer")) {
                    IntegerOperations.updateValue(stackEntry, value - 1, activationEntry);
                } else {
                    stackEntry.setValue(value - 1);
                }
                return tmpDecrement;
        }
        throw new RuntimeException("IDontKnowWhatJustHappenedException");
    }

    @Override
    public StackEntry visit(EnclosedExpr n, ActivationEntry activationEntry) {
        if (!n.getInner().isPresent()) {
            throw new RuntimeException("IDontKnowWhatJustHappenedException");
        }
        return n.getInner().get().accept(this, activationEntry);
    }

    @Override
    public StackEntry visit(ConditionalExpr n, ActivationEntry activationEntry) {
        int value = n.getCondition().accept(this, activationEntry).getIntValue();
        return value != 0 ? n.getThenExpr().accept(this, activationEntry) : n.getElseExpr().accept(this, activationEntry);
    }

    @Override
    public StackEntry visit(NullLiteralExpr n, ActivationEntry arg) {
        return new StackEntry(0, "Integer");
    }

    @Override
    public StackEntry visit(MethodCallExpr n, ActivationEntry arg) {
        if (n.getNameAsString().equals("print")) {
            String output = n.getArguments().get(0).accept(stringExpressionVisitor, arg);
            System.out.print(output);
            return null;
        } else if (n.getNameAsString().equals("println")) {
            String output = n.getArguments().isEmpty() ? "" : n.getArguments().get(0).accept(stringExpressionVisitor, arg);
            System.out.println(output);
            return null;
        } else if (n.getNameAsString().equals("nextInt")) {
            return new StackEntry(s.nextInt());
        } else if (n.getNameAsString().equals("hasNextInt")) {
            return new StackEntry(s.hasNextInt() ? 1 : 0);
        }

        List<StackEntry> arguments = new ArrayList<>();
        List<String> argumentDataTypes = new ArrayList<>();
        for (Expression expression : n.getArguments()) {
            StackEntry argument = expression.accept(this, arg);
            arguments.add(argument);
            argumentDataTypes.add(argument.getDataType());
        }
        MethodDeclaration methodDeclaration = MethodIndex.getInstance().findMethod(n.getNameAsString(), argumentDataTypes);
        StackEntry result = new StackEntry(0, methodDeclaration.getType().accept(dataTypeVisitor, null));
        ActivationEntry activationEntry = new ActivationEntry(arg);
        for (int i = 0; i < arguments.size(); i++) {
            Parameter parameter = methodDeclaration.getParameter(i);
            StackEntry parameterVariable = new StackEntry(0, parameter.getType().accept(dataTypeVisitor, null));
            assign(parameterVariable, arguments.get(i), activationEntry);
            activationEntry.addVariable(parameter.getNameAsString(), parameterVariable);
        }
        if (!methodDeclaration.getBody().isPresent()) {
            throw new RuntimeException("abstract methods not supported");
        } else {
            try {
                methodDeclaration.getBody().get().accept(this, activationEntry);
            } catch (ReturnException e) {
                assign(result, e.getReturnValue(), activationEntry);
                return result;
            }
        }
        throw new RuntimeException("missing return statement");
    }

    private void assign(StackEntry variable, StackEntry rightSide, ActivationEntry activationEntry) {
        int rightSideValue = rightSide.getIntValue();
        if (variable.getDataType().equals(rightSide.getDataType())) {
            variable.value = rightSide.getValue();
        } else if (variable.getDataType().equals("Integer")) {
            IntegerOperations.updateValue(variable, rightSideValue, activationEntry);
        } else {
            variable.value = rightSideValue;
        }
    }

    @Override
    public StackEntry visit(AssignExpr n, ActivationEntry activationEntry) {
        StackEntry variable = n.getTarget().accept(this, activationEntry);
        StackEntry rightSide = n.getValue().accept(this, activationEntry);
        int variableValue = variable.getIntValue();
        int rightSideValue = rightSide.getIntValue();
        switch (n.getOperator()) {
            case ASSIGN:
                assign(variable, rightSide, activationEntry);
                break;
            case PLUS:
                if (variable.getDataType().equals("Integer")) {
                    IntegerOperations.updateValue(variable, variableValue + rightSideValue, activationEntry);
                } else {
                    variable.value += rightSideValue;
                }
                break;
            case MINUS:
                if (variable.getDataType().equals("Integer")) {
                    IntegerOperations.updateValue(variable, variableValue - rightSideValue, activationEntry);
                } else {
                    variable.value -= rightSideValue;
                }
                break;
            case MULTIPLY:
                if (variable.getDataType().equals("Integer")) {
                    IntegerOperations.updateValue(variable, variableValue * rightSideValue, activationEntry);
                } else {
                    variable.value *= rightSideValue;
                }
                break;
            case DIVIDE:
                if (variable.getDataType().equals("Integer")) {
                    IntegerOperations.updateValue(variable, variableValue / rightSideValue, activationEntry);
                } else {
                    variable.value /= rightSideValue;
                }
                break;
            case AND:
                if (variable.getDataType().equals("Integer")) {
                    IntegerOperations.updateValue(variable, variableValue & rightSideValue, activationEntry);
                } else {
                    variable.value &= rightSideValue;
                }
                break;
            case OR:
                if (variable.getDataType().equals("Integer")) {
                    IntegerOperations.updateValue(variable, variableValue | rightSideValue, activationEntry);
                } else {
                    variable.value |= rightSideValue;
                }
                break;
            case XOR:
                if (variable.getDataType().equals("Integer")) {
                    IntegerOperations.updateValue(variable, variableValue ^ rightSideValue, activationEntry);
                } else {
                    variable.value ^= rightSideValue;
                }
                break;
            case REMAINDER:
                if (variable.getDataType().equals("Integer")) {
                    IntegerOperations.updateValue(variable, variableValue % rightSideValue, activationEntry);
                } else {
                    variable.value %= rightSideValue;
                }
                break;
            case LEFT_SHIFT:
                if (variable.getDataType().equals("Integer")) {
                    IntegerOperations.updateValue(variable, variableValue << rightSideValue, activationEntry);
                } else {
                    variable.value <<= rightSideValue;
                }
                break;
            case SIGNED_RIGHT_SHIFT:
                if (variable.getDataType().equals("Integer")) {
                    IntegerOperations.updateValue(variable, variableValue >> rightSideValue, activationEntry);
                } else {
                    variable.value >>= rightSideValue;
                }
                break;
            case UNSIGNED_RIGHT_SHIFT:
                if (variable.getDataType().equals("Integer")) {
                    IntegerOperations.updateValue(variable, variableValue >>> rightSideValue, activationEntry);
                } else {
                    variable.value >>>= rightSideValue;
                }
                break;
        }
        return variable;
    }

    @Override
    public StackEntry visit(CompilationUnit n, ActivationEntry activationEntry) {
        for (Node node : n.getChildNodes()) {
            node.accept(this, new ActivationEntry(activationEntry));
        }
        return null;
    }

    @Override
    public StackEntry visit(ClassOrInterfaceDeclaration n, ActivationEntry activationEntry) {
        for (Node node : n.getChildNodes()) {
            node.accept(this, new ActivationEntry(activationEntry));
        }
        return null;
    }

    @Override
    public StackEntry visit(MethodDeclaration n, ActivationEntry activationEntry) {
        try {
            if (n.getBody().isPresent()) {
                n.getBody().get().accept(this, activationEntry);
            }
        } catch (ReturnException e) {
        }
        return null;
    }

    @Override
    public StackEntry visit(BreakStmt n, ActivationEntry activationEntry) {
        throw new BreakException();
    }

    @Override
    public StackEntry visit(ReturnStmt n, ActivationEntry activationEntry) {
        if (n.getExpression().isPresent()) {
            throw new ReturnException(n.getExpression().get().accept(this, activationEntry));
        }
        throw new ReturnException();
    }

    @Override
    public StackEntry visit(IfStmt n, ActivationEntry activationEntry) {
        int value = n.getCondition().accept(this, activationEntry).getValue();
        if (value != 0) {
            n.getThenStmt().accept(this, new ActivationEntry(activationEntry));
        } else {
            if (n.getElseStmt().isPresent()) {
                n.getElseStmt().get().accept(this, new ActivationEntry(activationEntry));
            }
        }
        return null;
    }

    @Override
    public StackEntry visit(WhileStmt n, ActivationEntry activationEntry) {
        while (n.getCondition().accept(this, activationEntry).getValue() != 0) {
            try {
                n.getBody().accept(this, new ActivationEntry(activationEntry));
            } catch (BreakException e) {
                break;
            } catch (ContinueException e) {
                continue;
            }
        }
        return null;
    }

    @Override
    public StackEntry visit(ContinueStmt n, ActivationEntry activationEntry) {
        throw new ContinueException();
    }

    @Override
    public StackEntry visit(DoStmt n, ActivationEntry activationEntry) {
        do {
            try {
                n.getBody().accept(this, new ActivationEntry(activationEntry));
            } catch (BreakException e) {
                break;
            } catch (ContinueException e) {
                continue;
            }
        } while (n.getCondition().accept(this, activationEntry).getValue() != 0);
        return null;
    }

    @Override
    public StackEntry visit(ForStmt n, ActivationEntry activationEntryOld) {
        ActivationEntry activationEntry = new ActivationEntry(activationEntryOld);
        for (Expression expression : n.getInitialization()) {
            expression.accept(this, activationEntry);
        }
        while (!n.getCompare().isPresent() || n.getCompare().get().accept(this, activationEntry).getValue() != 0) {
            try {
                n.getBody().accept(this, activationEntry);
            } catch (BreakException e) {
                break;
            } catch (ContinueException e) {
            }
            for (Expression expression : n.getUpdate()) {
                expression.accept(this, activationEntry);
            }
        }
        return null;
    }

    @Override
    public StackEntry visit(VariableDeclarator n, ActivationEntry activationEntry) {
        String dataType = n.getType().accept(dataTypeVisitor, null);
        if (!dataType.equals("int") && !dataType.equals("Integer")) {
            throw new RuntimeException("Unsupported data type" + dataType);
        }
        int value = 0;
        if (n.getInitializer().isPresent()) {
            StackEntry rightSide = n.getInitializer().isPresent() ? n.getInitializer().get().accept(this, activationEntry) : new StackEntry(0);
            if (dataType.equals(rightSide.getDataType())) {
                value = rightSide.getValue();
            } else if (dataType.equals("Integer")) {
                value = IntegerOperations.createInteger(rightSide.getValue(), activationEntry);
            } else {
                value = rightSide.getIntValue();
            }
        }
        activationEntry.addVariable(n.getName().getIdentifier(), dataType, value);
        return null;
    }

    @Override
    public StackEntry visit(VariableDeclarationExpr n, ActivationEntry activationEntry) {
        for (VariableDeclarator variableDeclarator : n.getVariables()) {
            variableDeclarator.accept(this, activationEntry);
        }
        return null;
    }

    @Override
    public StackEntry visit(BlockStmt n, ActivationEntry activationEntryOld) {
        ActivationEntry activationEntry = new ActivationEntry(activationEntryOld);
        final NodeList<Statement> statements = n.getStatements();
        for (Statement statement : statements) {
            statement.accept(this, activationEntry);
        }
        return null;
    }

    @Override
    public StackEntry visit(ExpressionStmt n, ActivationEntry activationEntry) {
        return n.getExpression().accept(this, activationEntry);
    }

    @Override
    public StackEntry visit(PackageDeclaration n, ActivationEntry activationEntry) {
        return null;
    }

    @Override
    public StackEntry visit(LineComment n, ActivationEntry activationEntry) {
        return null;
    }

    @Override
    public StackEntry visit(BlockComment n, ActivationEntry activationEntry) {
        return null;
    }

    @Override
    public StackEntry visit(ClassOrInterfaceType n, ActivationEntry activationEntry) {
        return null;
    }

    @Override
    public StackEntry visit(SimpleName n, ActivationEntry arg) {
        return null;
    }

}
