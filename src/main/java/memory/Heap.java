package memory;

public class Heap {

    private int memorySize;
    private int[] memory;
    private boolean[] occupied;
    private int pointer = 0;

    private static Heap instance;

    public static Heap getInstance() {
        if (instance == null) {
            instance = new Heap();
        }
        return instance;
    }

    private Heap() {
    }

    public void init(int memorySize) {
        this.memorySize = memorySize;
        this.memory = new int[memorySize];
        this.occupied = new boolean[memorySize];
        // 0 is null! don't touch 0
        occupied[0] = true;
    }

    private int findFreeSlot() {
        int i = pointer;
        do {
            if (!occupied[i]) {
                occupied[i] = true;
                pointer = i;
                return i;
            }
            i = (i + 1) % memorySize;
        } while (i != pointer);
        pointer = 1;
        return 0;
    }

    private void garbageCollection(ActivationEntry activationEntryBottom) {
        System.out.println("MUHAHA, I'm collecting garbage!!!");
        for (int i = 1; i < memorySize; i++) {
            occupied[i] = false;
        }
        ActivationEntry activationEntry = activationEntryBottom;
        while (activationEntry != null) {
            activationEntry.markOccupied(occupied);
            activationEntry = activationEntry.getParent();
        }
    }

    public int allocate(ActivationEntry activationEntry) {
        int reference = findFreeSlot();
        if (reference == 0) {
            garbageCollection(activationEntry);
            reference = findFreeSlot();
            if (reference == 0) {
                throw new OutOfMemoryError();
            }
        }
        return reference;
    }


    public int getValue(int i) {
        if (i == 0) {
            throw new NullPointerException();
        }
        if (i < 0 || i >= memorySize || !occupied[i]) {
            throw new RuntimeException("IDontKnowWhatJustHappenedException");
        }
        return memory[i];
    }

    public void setValue(int i, int value) {
        if (i < 0 || i >= memorySize || !occupied[i]) {
            throw new RuntimeException("IDontKnowWhatJustHappenedException");
        }
        memory[i] = value;
    }

}
