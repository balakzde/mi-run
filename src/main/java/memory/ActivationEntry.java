package memory;

import java.util.HashMap;
import java.util.Map;

public class ActivationEntry {
    private ActivationEntry parent;

    private Map<String, StackEntry> integerVariables = new HashMap<>();

    public ActivationEntry() {
        parent = null;
    }

    public ActivationEntry(ActivationEntry parent) {
        this.parent = parent;
    }

    public StackEntry findVariable(String variable) {
        ActivationEntry activationEntry = this;
        while(activationEntry != null) {
            StackEntry stackEntry = activationEntry.integerVariables.get(variable);
            if (stackEntry != null) {
                return stackEntry;
            }
            activationEntry = activationEntry.parent;
        }
        throw new RuntimeException("IDontKnowWhatJustHappenedException");
    }

    public void addVariable(String variable, String dataType, int value) {
        addVariable(variable, new StackEntry(value, dataType));
    }

    public void addVariable(String variable, StackEntry stackEntry) {
        integerVariables.put(variable, stackEntry);
    }

    public void markOccupied(boolean[] occupied){
        for (StackEntry stackEntry : integerVariables.values()) {
            if(stackEntry.getDataType().equals("Integer")){
                occupied[stackEntry.getValue()] = true;
            }
        }
    }

    public ActivationEntry getParent() {
        return parent;
    }

    @Override
    public String toString() {
        return "ActivationEntry{" +
                "integerVariables=" + integerVariables +
                ", parent=" + parent +
                '}';
    }
}
