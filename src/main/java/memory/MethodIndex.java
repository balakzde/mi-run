package memory;

import com.github.javaparser.ast.body.MethodDeclaration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MethodIndex {

    private static class Method {
        List<String> parameterTypes;
        MethodDeclaration node;

        public Method(List<String> parameterTypes, MethodDeclaration node) {
            this.parameterTypes = parameterTypes;
            this.node = node;
        }
    }

    Map<String, Map<Integer, List<Method>>> index = new HashMap<>();
    MethodDeclaration mainNode;

    private static MethodIndex instance;

    public static MethodIndex getInstance() {
        if (instance == null) {
            instance = new MethodIndex();
        }
        return instance;
    }

    private MethodIndex() {
    }

    public void addMethod(List<String> parameters, MethodDeclaration node) {
        String name = node.getNameAsString();
        int arity = parameters.size();
        Map<Integer, List<Method>> methodsByName = index.get(name);
        if (methodsByName == null) {
            methodsByName = new HashMap<>();
            index.put(name, methodsByName);
        }
        List<Method> methodsWithArity = methodsByName.get(arity);
        if (methodsWithArity == null) {
            methodsWithArity = new ArrayList<>();
            methodsByName.put(arity, methodsWithArity);
        }
        methodsWithArity.add(new Method(parameters, node));
    }

    public MethodDeclaration findMethod(String name, List<String> argumentTypes) {
        final List<Method> methods = index.get(name).get(argumentTypes.size());
        final List<MethodDeclaration> possibleMethods = new ArrayList<>();
        for (Method method : methods) {
            if (method.parameterTypes.equals(argumentTypes)) {
                return method.node;
            }
            possibleMethods.add(method.node);
        }
        if (possibleMethods.size() == 1) {
            return possibleMethods.get(0);
        }else if( possibleMethods.size()>1){
            throw new RuntimeException("Too many possible methods");
        }

        throw new NoSuchMethodError();
    }

    public MethodDeclaration getMainNode() {
        return mainNode;
    }

    public void setMainNode(MethodDeclaration mainNode) {
        this.mainNode = mainNode;
    }
}
