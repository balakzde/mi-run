package memory;

import memory.Heap;

public class StackEntry {
    public int value;
    private String dataType;

    public StackEntry(int value, String dataType) {
        this.value = value;
        this.dataType = dataType;
    }

    public StackEntry(int value) {
        this(value, "int");
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getDataType() {
        return dataType;
    }

    public int getIntValue() {
        if (dataType.equals("int")) {
            return value;
        } else if (dataType.equals("Integer")) {
            return Heap.getInstance().getValue(value);
        }
        throw new RuntimeException("Unsupported data type " + dataType);
    }

    @Override
    public String toString() {
        return "StackEntry{" +
                "value=" + value +
                ", dataType='" + dataType + '\'' +
                '}';
    }
}
