# Interpreter zjednodušené a trochu poupravené verze javy
Postaven na [javaparseru](https://github.com/javaparser/javaparser) 

 Podporuje:
* proměnné: Integer, int
* cykly: while, for, do while
* větvení: if, ternární operátor, break, continue, return
* metody: statické (mimo void)
* správu paměti: garbage collector M&S
* operátory: všechny
* výpis: System.out.print[ln]

Odlišnosti od jazyka java
* boolean se vyhodnocuje jako v C
* == implementován jako equals
* pro čtení podporuje vestavěnou funkci nextInt()
 

## Požadavky
java 1.7 a novější
maven
 
## Instalace
maven install
 
## Spuštění
* podporuje parametr pro specifikaci velikosti haldy -Xmx (např -Xmx2g)

./javarun [-XmxXX] soubor.java
